from PyQt4 import QtGui, QtCore
import sys, math
import cmath

length = 1200
width = 600

screen_position = length - 200  
position = []

## Функция расчета длины оптического пути
# @param source_place положениие источника
# @param slit_place положение щели в дифракционной решетке
# @param pos_y координата на экране
# @return длина оптического пути
def count_L(source_place,slit_place,pos_y):
       l1 = math.sqrt((source_place[0]-slit_place[0])*(source_place[0]-slit_place[0]) + (source_place[1]-slit_place[1])*(source_place[1]-slit_place[1]))
       l2 = math.sqrt((slit_place[0]-screen_position)*(slit_place[0]-screen_position) + (slit_place[1]-pos_y)*(slit_place[1]-pos_y))
       L = l1+l2
       return L

## Функция расчета амплитуды
# @param l длина оптического пути
# @return амплитуда
def count_A(l):
    return 1/(l*l)

## Функция расчета разности фаз
# @param L длина оптического пути
# @param lamda длина волны источника света
# @return разность фаз
def count_F(L,lamda):
    return 2*(math.pi)*L/lamda


class MyWidget(QtGui.QWidget):
    ## Конструктор   
    def __init__(self):
        super().__init__()
        self.I = []
        self.sources = []
        self.setGeometry(100,100,length,width)
        self.setWindowTitle('DEMONSTRATION')
        self.flag = 'none'
        self.contrast = 1
    #source
        self.source_number = 0
        self.wavelength = 5
    #hole
        self.number = 0
        self.hole_position = []
        self.holes = []
    #colour button
        self.col_but = QtGui.QRadioButton('COLOUR',self)
        self.col_but.resize(self.col_but.sizeHint())
        self.col_but.move(length/4,0)
        self.col_but.clicked.connect(self.show_color)
    #amplitude button
        self.amplit_but = QtGui.QRadioButton('AMPLITUDE',self)
        self.amplit_but.resize(self.amplit_but.sizeHint())
        self.amplit_but.move(length/2,0)
        self.amplit_but.clicked.connect(self.show_ampl)
    #source menu button
        self.source_button = QtGui.QPushButton('SOURCE',self)
        self.source_button.resize(self.source_button.sizeHint())
        self.source_button.move(length-150,width/10)
        menu_source = QtGui.QMenu(self)
        menu_source.addAction('add', self.add_source)
        menu_source.addAction('set wavelength',self.set_wavelength)
        self.source_button.setMenu(menu_source)
    #hole menu button
        self.hole_button = QtGui.QPushButton('HOLE',self)
        self.hole_button.resize(self.hole_button.sizeHint())
        self.hole_button.move(length-150,3*(width/10))
        menu_hole = QtGui.QMenu(self)
        menu_hole.addAction('add', self.add_hole)
        menu_hole.addAction('set number',self.set_number)
        self.hole_button.setMenu(menu_hole)
    #delete button
        self.del_but = QtGui.QPushButton('DELETE',self)
        self.del_but.resize(self.del_but.sizeHint())
        self.del_but.move(length-150,8*(width/10))
        self.del_but.clicked.connect(self.del_source)
    #cancel button
        self.c_but = QtGui.QPushButton('CANCEL',self)
        self.c_but.resize(self.c_but.sizeHint())
        self.c_but.move(length-150,9*(width/10))
        self.c_but.clicked.connect(self.cancel)
    #show button
        self.sh_but = QtGui.QPushButton('SHOW',self)
        self.sh_but.resize(self.sh_but.sizeHint())
        self.sh_but.move(length-150,width/2)
        self.sh_but.clicked.connect(self.points)
    #contrast button
        self.c_but = QtGui.QPushButton('CONTRAST',self)
        self.c_but.resize(self.c_but.sizeHint())
        self.c_but.move(length-150,7*(width/10))
        self.c_but.clicked.connect(self.set_contrast)

    ## Обработка нажатий мыши
    # @param q объект типа QMouseEvent
    def mousePressEvent(self,q):
    #add source
        if self.flag == 'source':
            self.sources.append((q.x(),q.y()))
            self.source_number += 1
            self.flag = 'none'
            self.update()
            print('sources: ',self.sources)
    #delete
        if self.flag == 'delete':
            position = ((q.x(),q.y()))
            print('source num = ',self.source_number)
            print('before delete: ',position)
            for i in range (self.source_number):
                print('i= ',i)
                if (self.sources[i][0] == position[0]) or (self.sources[i][0] == position[0]+1) or (self.sources[i][0] == position[0]-1) or (self.sources[i][0] == position[0]+2) or (self.sources[i][0] == position[0]-2):
                   if (self.sources[i][1] == position[1]) or (self.sources[i][1] == position[1]+1) or (self.sources[i][1] == position[1]-1) or (self.sources[i][1] == position[1]+2) or (self.sources[i][1] == position[1]-2):
                       self.sources.pop(i)
                       self.source_number -= 1
                       print('source new num = ',self.source_number)
                       self.update()
                       break

            self.flag = 'none'
            print('delete')
            print('souces arter delete: ',self.sources)
    #add hole
        if self.flag == 'hole':
            self.hole_position.append((q.x(),q.y()))
            print('hole: ',self.hole_position)
            self.update()
            if len(self.hole_position) == 2:
                self.flag = 'none'


    ## Функция добавления источника света 
    def add_source(self):
        print('in add_source')
        self.flag = 'source'

    ## Функция для задания длины волны источников света с помощью диалогового окна
    def set_wavelength(self):
        self.wavelength, ok  = QtGui.QInputDialog.getText(self, 'set wawelength in nm', 'wavelength:')
        self.wavelength = float(self.wavelength)
        self.wavelength = self.wavelength
        print('wavelength = ',self.wavelength)

    ## Функция размещения дифракционной решетки
    def add_hole(self):
        print('in add_hole')
        self.hole_position = []
        self.flag = 'hole'

    ## Функция для задания количества щелей в дифракционной решетке с помощью диалогового окна
    def set_number(self):
        self.number, ok  = QtGui.QInputDialog.getText(self, 'number of holes', 'set number:')
        self.number = int(self.number)
        self.holes = []
        for i in range (1,self.number+1):
            x = self.hole_position[0][0]*i/(self.number+1) + self.hole_position[1][0]*(self.number+1-i)/(self.number+1)
            y = self.hole_position[0][1]*i/(self.number+1) + self.hole_position[1][1]*(self.number+1-i)/(self.number+1)
            self.holes.append((x,y))
        print('self.holes: ', self.holes)
        self.update()

    ## Функция отмены случайного нажатия на кнопку  
    def cancel(self):
        print('cancel')
        self.flag = 'none'

    ## Функция удаления источника света
    def del_source(self):
        print('in del_source')
        self.flag = 'delete'

    ## Функция для задания контрастности дифракционной картины с помощью диалогового окна
    def set_contrast(self):
        self.contrast, ok  = QtGui.QInputDialog.getText(self, 'contrast', 'set contrast:')
        self.contrast = float(self.contrast)
        print("contrast = ",self.contrast)

    ##  Функция при выборе кнопки "COLOR"
    def show_ampl(self):
           print("in show ampl")
           self.picture = "amplitude"
           self.update()

    ##  Функция при выборе кнопки "AMLITUDE"
    def show_color(self):
           print("in show color")
           self.picture = "color"
           self.update()
       
           
    ## Функция расчета отнормированной интенсивности для конкретной координаты на экране
    # @param pos_y координата на экране   
    def counting(self, pos_y):
        E = 0
        a = 0
        for i in range(self.source_number):
               for j in range(self.number):
                      l = count_L(self.sources[i],self.holes[j],pos_y)
                      f = count_F(l,self.wavelength)
                      A = count_A(l)
                      F = cmath.exp(1j*f)
                      E += F*A                  
                      a += A
        I = abs(E)*abs(E)
        i = I/(a*a)
        return i

    ##  Функция расчета интенсивностей для точек на экране
    def points(self):
           self.I = []
           for j in range(100,width-100,3):
                  i = self.counting(j)
                  self.I.append((j,i))
           self.flag = "draw"
           self.update()
              
    ## Рисование
    # @param e объект типа QPaintEvent 
    def paintEvent(self,e):
        p = QtGui.QPainter()
        p.begin(self)
        p.setPen(QtGui.QPen(QtGui.QColor(128,128,128),1))
        p.drawLine(0,width/2,screen_position,width/2) #main optical axis
    #screen
        p.setPen(QtGui.QPen(QtGui.QColor(0,0,0),3))
        p.drawLine(screen_position,50,screen_position,width-50)
        for i in range(100,width-50,50):
            p.drawLine(screen_position,i,screen_position+30,i-30)
    #sources
        for i in range(self.source_number):
            p.drawEllipse(self.sources[i][0],self.sources[i][1],2,2)
    #hole
        if(len(self.hole_position) == 2):
            p.drawLine(self.hole_position[0][0],self.hole_position[0][1],self.hole_position[1][0],self.hole_position[1][1])
        p.setPen(QtGui.QPen(QtGui.QColor(255,255,255),3))
        for i in range (self.number):
            p.drawRect(self.holes[i][0]-1,self.holes[i][1]-1,2,2)
    #picture
        if(self.flag == "draw"):
               if(self.picture == "amplitude"):
                      for i in range(len(self.I)):
                           color = 255*(self.I[i][1]*self.contrast)
                           p.setPen(QtGui.QPen(QtGui.QColor(0,0,200),3))
                           distance = screen_position - (color/10)
                           p.drawLine(distance,self.I[i][0],screen_position,self.I[i][0])
                           p.setPen(QtGui.QPen(QtGui.QColor(255,0,0),3))
                           p.drawPoint(distance,self.I[i][0])
                           self.flag = "none"
               if(self.picture == "color"):
                      for i in range(len(self.I)):
                           color = 255*(self.I[i][1]*self.contrast)
                           if(color > 255):
                                  p.setPen(QtGui.QPen(QtGui.QColor(200,0,0),3))
                           else:
                                  p.setPen(QtGui.QPen(QtGui.QColor(color,color,color),3))
                           p.drawLine(screen_position-10,self.I[i][0],screen_position,self.I[i][0])
                           self.flag = "none"
        p.end()


def main():
    app = QtGui.QApplication(sys.argv)
    w = MyWidget()
    w.show()
    sys.exit(app.exec_())

if __name__== '__main__':
    main()
